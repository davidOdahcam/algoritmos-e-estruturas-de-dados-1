import time
import random
from linkedList import LinkedList

capacities = [10, 100, 1000, 10000, 100000, 1000000]

# Fazendo com que todos os valores solicitados pelo professor sejam analisados em uma única execução
for c in capacities:
  # Instanciando uma lista encadeada
  linkedList = LinkedList(capacity = c)

  counter = 0
  # Marcando o início da execução
  begin = int(round(time.time() * 1000))
  for i in range(linkedList.capacity):
    # Gerando um valor randomico
    element = random.randint(0, linkedList.capacity)
    # Adicionando o valor à lista
    linkedList.addBegin(element)
    counter+=1
  
  # Marcando o fim da execução
  end = int(round(time.time() * 1000))
  # Imprimindo o tempo de execução
  print('\nTempo de execução para inserir {} elementos: {}ms'.format(linkedList.capacity, (end - begin)))

  # Marcando o início da execução
  begin = int(round(time.time() * 1000))
  # Ordenando a lista
  linkedList.bubbleSort()
  # Marcando o fim da execução
  end = int(round(time.time() * 1000))
  # Imprimindo o tempo de execução
  print('Tempo de execução para ordenar {} elementos: {}ms'.format(linkedList.capacity, (end - begin)))

  # Marcando o início da execução
  begin = int(round(time.time() * 1000))
  # Removendo da lista
  for i in range(linkedList.node_count):
    linkedList.remove(linkedList.head.data)
  # Marcando o fim da execução
  end = int(round(time.time() * 1000))
  # Imprimindo o tempo de execução
  print('Tempo de execução para remover {} elementos: {}ms'.format(linkedList.capacity, (end - begin)))

  del linkedList