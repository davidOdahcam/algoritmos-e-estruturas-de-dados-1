# Definindo a classe dos nós
class Node:
  # Construtor
  def __init__(self, data = 0, next_node = None):
    self.data = data
    self.next_node = next_node
  
  def __str__(self):
    return str(self.data)