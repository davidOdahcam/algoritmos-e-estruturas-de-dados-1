from node import Node

# Definindo a classe da lista encadeada
class LinkedList:
  # Contrutor
  def __init__(self, head = None, node_count = 0, capacity = 0):
    self.head = head
    self.node_count = node_count
    self.capacity = capacity
  
  # Função que adiciona um elemento ao início
  def addBegin(self, element = None):
    # Instanciando um novo nó
    node = Node(element)
    node.next_node = self.head
    self.head = node
    self.node_count = self.node_count + 1

  def remove(self, element = None):
    # Nó a ser removido é a cabeça da lista.
    if self.head.data == element:
      self.head = self.head.next_node
    else:
      previous = None
      current = self.head
      while current and current.data != element:
        previous = current
        current = current.next_node
      if current:
        self.node_count = self.node_count - 1
        previous.next_node = current.next_node
      else:
        previous.next_node = None
  
  # Função que ordena a lista
  def bubbleSort(self):
    for i in range(self.node_count - 1):
      current = self.head
      nxt = current.next_node
      previous = None
      while nxt:#Comparisons in passes
        if current.data>nxt.data:
          if previous == None:
            previous = current.next_node
            nxt = nxt.next_node
            previous.next_node = current
            current.next_node = nxt
            self.head = previous
          else:   
            temp = nxt
            nxt = nxt.next_node
            previous.next_node = current.next_node
            previous = temp
            temp.next_node = current
            current.next_node = nxt
        else:           
          previous = current
          current = nxt
          nxt = nxt.next_node
      i+=1