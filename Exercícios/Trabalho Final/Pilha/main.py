import time
import random
from stack import Stack

capacities = [10, 100, 1000, 10000, 100000, 1000000]

# Fazendo com que todos os valores solicitados pelo professor sejam analisados em uma única execução
for c in capacities:
    ### Início da execução em uma pilha
    # Instanciando uma pilha
    stack = Stack(c)

    # Marcando o início da execução
    begin = time.time()

    for i in range(stack.capacity):
        # Função que insere um item à pilha
        stack.push(random.randint(0, stack.capacity))

    # Marcando o fim da execução
    end = time.time()

    # Imprimindo o tempo de execução
    print('Tempo de execução para inserir {} elementos: {}ms'.format(stack.capacity, (end - begin)))

    # Marcando o início da execução
    begin = time.time()

    for i in range(stack.capacity):
        # Função que insere um item à pilha
        stack.pop()
    
    # Marcando o fim da execução
    end = time.time()

    # Imprimindo o tempo de execução
    print('Tempo de execução para remover {} elementos: {}ms\n'.format(stack.capacity, (end - begin)))
    
    # Desfazendo pilha
    del stack
    ### Fim da execução em uma pilha