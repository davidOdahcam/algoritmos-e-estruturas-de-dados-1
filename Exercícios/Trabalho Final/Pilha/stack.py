class Stack:
  # Construtor
  def __init__(self, capacity):
    self.items = []
    self.capacity = capacity

  def isEmpty(self):
    return self.items == []

  def push(self, item):
    if (len(self.items) < self.capacity):
      self.items.append(item)
    else:
      print('Overflow')

  def pop(self):
    if (self.isEmpty != True):
      self.items.pop()
    else:
      print('Underflow')

  def peek(self):
    return self.items[len(self.items)-1]

  def size(self):
    return len(self.items)

  def show(self):
    if (self.isEmpty()):
      print('A pilha está vazia!')
    else:
      for i in self.items[::-1]:
        print(i)