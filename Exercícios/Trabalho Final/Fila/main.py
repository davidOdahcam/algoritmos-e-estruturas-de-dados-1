import time
import random
from fila import Queue

capacities = [10, 100, 1000, 10000, 100000, 1000000]

# Fazendo com que todos os valores solicitados pelo professor sejam analisados em uma única execução
for c in capacities:
    ### Início da execução em uma fila
    # Instanciando uma fila
    queue = Queue(c)

    # Marcando o início da execução
    begin = time.time()

    for i in range(queue.capacity):
        # Função que adiciona item à fila
        queue.enqueue(random.randint(0, queue.capacity))

    # Realizando o cálculo de tempo de execução
    end = time.time()

    # Imprimindo o tempo de execução
    print('Tempo de execução para inserir {} elementos: {}ms'.format(queue.capacity, (end - begin)))

    # Marcando o início da execução
    begin = time.time()

    for i in range(queue.capacity):
        # Função que remove item da fila
        queue.dequeue()
    
    # Realizando o cálculo de tempo de execução
    end = time.time()

    # Imprimindo o tempo de execução
    print('Tempo de execução para remover {} elementos: {}ms\n'.format(queue.capacity, (end - begin)))

    # Desfazendo fila
    del queue
    ### Fim da execução em uma fila