CAPACITY = 100

class Queue:
  def __init__(self):
    self.items = []
    self.capacity = CAPACITY

  def isEmpty(self):
    return self.items == []

  def enqueue(self, item):
    self.items.insert(0,item)

  def dequeue(self):
    return self.items.pop()

  def size(self):
    return len(self.items)

  def search(self, item):
    for i in range(len(self.items)):
      if (item == self.items[i]):
        print('O elemento {} existe e está na posição {}'.format(item, i))
        return
    print('O elemento não foi encontrado!')
  
  def show(self):
    for i in self.items[::-1]:
      print(i, end="|")